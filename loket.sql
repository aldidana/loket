CREATE DATABASE IF NOT EXISTS madgeek;

CREATE TABLE "locations" (
	"id" SERIAL NOT NULL PRIMARY KEY,
	"name" VARCHAR
);

CREATE TABLE "events" (
	"id" SERIAL NOT NULL PRIMARY KEY,
	"name" VARCHAR,
	"start_at" TIMESTAMP NOT NULL,
	"end_at" TIMESTAMP NOT NULL,
	"location_id" int NOT NULL REFERENCES "locations" ("id")
);

CREATE TABLE "ticket_types" (
	"id" SERIAL NOT NULL PRIMARY KEY,
	"name" VARCHAR,
	"event_id" INT NOT NULL REFERENCES "events" ("id"),
	"price" NUMERIC NOT NULL,
	"quota" INT NOT NULL
);

CREATE TABLE "customers" (
	"id" SERIAL NOT NULL PRIMARY KEY,
	"email" VARCHAR UNIQUE
);

CREATE TABLE "customers" (
	"id" SERIAL NOT NULL PRIMARY KEY,
	"email" VARCHAR UNIQUE
);

CREATE TABLE "transactions" (
	"id" SERIAL NOT NULL PRIMARY KEY,
	"ticket_id" INT NOT NULL REFERENCES "ticket_types" ("id"),
	"customer_id" INT NOT NULL REFERENCES "customers" ("id"),
	"total_price" NUMERIC NOT NULL,
	"quantity" INT NOT NULL
);

ALTER TABLE "transactions" DROP COLUMN "ticket_id", DROP COLUMN "quantity";

CREATE TABLE "purchase_details" (
	"id" SERIAL NOT NULL PRIMARY KEY,
	"transaction_id" INT NOT NULL REFERENCES "transactions" ("id"),
	"ticket_id" INT NOT NULL REFERENCES "ticket_types" ("id"),
	"quantity" INT NOT NULL,
	"total_price" INT NOT NULL
);