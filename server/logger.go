package server

import (
	"log"
	"net/http"
	"strings"
	"time"
)

type responseLogger struct {
	http.ResponseWriter
	statusCode int
}

func newResponseLogger(w http.ResponseWriter, r *http.Request) *responseLogger {
	return &responseLogger{w, http.StatusOK}
}

func (rl *responseLogger) WriteHeader(statusCode int) {
	rl.statusCode = statusCode
	rl.ResponseWriter.WriteHeader(statusCode)
}

//Logger is middleware for logger and trim trailing slash
func Logger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.URL.Path = strings.TrimRight(r.URL.Path, "/")

		start := time.Now()

		rl := newResponseLogger(w, r)
		handler.ServeHTTP(rl, r)

		log.Printf(
			"%s\t%s\t%v\t%v",
			r.Method,
			r.RequestURI,
			rl.statusCode,
			time.Since(start),
		)
	})
}
