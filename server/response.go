package server

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/aldidana/loket/repository"
)

type Response struct {
	Error   bool   `json:"error"`
	Message string `json:"message,omitempty"`
	Object  string `json:"object"`
}

type LocationResponse struct {
	Response
	ID   int    `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type EventResponse struct {
	Response
	ID       int        `json:"id,omitempty"`
	Name     string     `json:"name,omitempty"`
	Location string     `json:"location,omitempty"`
	StartAt  *time.Time `json:"start_at,omitempty"`
	EndAt    *time.Time `json:"end_at,omitempty"`
}

type TicketResponse struct {
	Response
	ID        int        `json:"id,omitempty"`
	Name      string     `json:"name,omitempty"`
	Price     int64      `json:"price,omitempty"`
	Quota     int        `json:"quota,omitempty"`
	EventName string     `json:"event_name,omitempty"`
	Location  string     `json:"location,omitempty"`
	StartAt   *time.Time `json:"start_at,omitempty"`
	EndAt     *time.Time `json:"end_at,omitempty"`
}

type TransactionResponse struct {
	Response
	ID               int                `json:"id,omitempty"`
	CustomerEmail    string             `json:"customer_email,omitempty"`
	GrandTotal       int64              `json:"grand_total,omitempty"`
	PurchasesDetails []PurchaseResponse `json:"purchases_details,omitempty"`
}

type PurchaseResponse struct {
	EventName     string     `json:"event_name,omitempty"`
	EventLocation string     `json:"event_location,omitempty"`
	EventStart    *time.Time `json:"event_start,omitempty"`
	EventEnd      *time.Time `json:"event_end,omitempty"`
	TicketName    string     `json:"ticket_name,omitempty"`
	TicketPrice   int64      `json:"ticket_price,omitempty"`
	Quantity      int        `json:"quantity,omitempty"`
	TotalPrice    int64      `json:"total_price,omitempty"`
}

//Render response json
func Render(w http.ResponseWriter, r *http.Request, value interface{}, statusCode int) {
	result, err := json.Marshal(value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(result)
}

//CreateTransactionResponse from Transaction repository
func CreateTransactionResponse(transactionResponse TransactionResponse, transactionDetails *repository.TransactionDetails) TransactionResponse {
	var purchaseResponses []PurchaseResponse

	for _, purchase := range transactionDetails.Purchases {
		purchaseResponses = append(purchaseResponses, PurchaseResponse{
			EventName:     purchase.Ticket.Event.Name,
			EventLocation: purchase.Ticket.Event.Location.Name,
			EventStart:    purchase.Ticket.Event.StartAt,
			EventEnd:      purchase.Ticket.Event.EndAt,
			TicketName:    purchase.Ticket.Name,
			TicketPrice:   purchase.Ticket.Price,
			Quantity:      purchase.Quantity,
			TotalPrice:    purchase.TotalPrice,
		})
	}

	transactionResponse.ID = transactionDetails.Transaction.ID
	transactionResponse.CustomerEmail = transactionDetails.Transaction.Customer.Email
	transactionResponse.GrandTotal = transactionDetails.Transaction.TotalPrice
	transactionResponse.PurchasesDetails = purchaseResponses

	return transactionResponse
}
