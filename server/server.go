package server

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/aldidana/loket/repository"
)

//Handler for http handler
type Handler struct {
	DB repository.Datastore
}

//NewHandler create new handler
func NewHandler(c *repository.Conn) *Handler {
	return &Handler{c}
}

//GetBody helper / Utils to get data from body or request form
func GetBody(w http.ResponseWriter, r *http.Request, requestData interface{}, responseData interface{}) (interface{}, error) {
	switch r.Header.Get("Content-Type") {
	case "application/json":
		if err := json.NewDecoder(r.Body).Decode(&requestData); err != nil {
			return nil, err
		}

		return requestData, nil
	case "application/x-www-form-urlencoded":
		req := make(map[string]interface{})
		for k, v := range r.Form {
			req[k] = v[0]
		}

		return req, nil
	default:
		return nil, errors.New(http.StatusText(http.StatusBadRequest))
	}
}
