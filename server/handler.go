package server

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/asaskevich/govalidator"
	"gitlab.com/aldidana/loket/repository"
)

//PostCreateLocation handler to create new location
func (h *Handler) PostCreateLocation(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}

	r.ParseForm()

	var locationRequest LocationRequest

	locationResponse := LocationResponse{
		Response: Response{
			Error:  false,
			Object: "Location",
		},
	}

	body, err := GetBody(w, r, locationRequest, locationResponse)
	if err != nil {
		locationResponse.Message = err.Error()
		locationResponse.Error = true
		Render(w, r, locationResponse, http.StatusBadRequest)
		return
	}

	req, err := json.Marshal(body)
	if err != nil {
		locationResponse.Message = err.Error()
		locationResponse.Error = true
		Render(w, r, locationResponse, http.StatusBadRequest)
		return
	}
	json.Unmarshal(req, &locationRequest)

	_, err = govalidator.ValidateStruct(locationRequest)
	if err != nil {
		locationResponse.Message = err.Error()
		locationResponse.Error = true
		Render(w, r, locationResponse, http.StatusBadRequest)
		return
	}

	location, err := h.DB.CreateLocation(locationRequest.Name)
	if err != nil {
		locationResponse.Message = err.Error()
		locationResponse.Error = true
		Render(w, r, locationResponse, http.StatusBadRequest)
		return
	}

	locationResponse.ID = location.ID
	locationResponse.Name = location.Name

	Render(w, r, locationResponse, http.StatusCreated)
}

//PostCreateEvent handler to create new event
func (h *Handler) PostCreateEvent(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}

	r.ParseForm()

	var eventRequest EventRequest

	eventResponse := EventResponse{
		Response: Response{
			Error:  false,
			Object: "Event",
		},
	}

	body, err := GetBody(w, r, eventRequest, eventResponse)
	if err != nil {
		eventResponse.Message = err.Error()
		eventResponse.Error = true
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}

	req, err := json.Marshal(body)
	if err != nil {
		eventResponse.Message = err.Error()
		eventResponse.Error = true
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}
	json.Unmarshal(req, &eventRequest)

	_, err = govalidator.ValidateStruct(eventRequest)
	if err != nil {
		eventResponse.Message = err.Error()
		eventResponse.Error = true
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}

	if diff := eventRequest.EndAt.Sub(eventRequest.StartAt).Seconds(); diff <= 0 {
		eventResponse.Message = errors.New("end_at time must be greater/late than start_at").Error()
		eventResponse.Error = true
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}

	event, err := h.DB.CreateEvent(eventRequest.Name, eventRequest.Location, eventRequest.StartAt, eventRequest.EndAt)
	if err != nil {
		eventResponse.Message = err.Error()
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}

	eventResponse.ID = event.ID
	eventResponse.Name = event.Name
	eventResponse.Location = event.Location.Name
	eventResponse.StartAt = event.StartAt
	eventResponse.EndAt = event.EndAt

	Render(w, r, eventResponse, http.StatusCreated)
}

//PostCreateTicket handler to create new ticket
func (h *Handler) PostCreateTicket(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}

	r.ParseForm()

	var ticketRequest TicketRequest

	ticketResponse := TicketResponse{
		Response: Response{
			Error:  false,
			Object: "Ticket",
		},
	}

	body, err := GetBody(w, r, ticketRequest, ticketResponse)
	if err != nil {
		ticketResponse.Message = err.Error()
		ticketResponse.Error = true
		Render(w, r, ticketResponse, http.StatusBadRequest)
		return
	}

	req, err := json.Marshal(body)
	if err != nil {
		ticketResponse.Message = err.Error()
		ticketResponse.Error = true
		Render(w, r, ticketResponse, http.StatusBadRequest)
		return
	}
	json.Unmarshal(req, &ticketRequest)

	_, err = govalidator.ValidateStruct(ticketRequest)
	if err != nil {
		ticketResponse.Message = err.Error()
		ticketResponse.Error = true
		Render(w, r, ticketResponse, http.StatusBadRequest)
		return
	}

	ticket, err := h.DB.CreateTicket(ticketRequest.Name, ticketRequest.EventID, ticketRequest.Price, ticketRequest.Quota)
	if err != nil {
		ticketResponse.Message = err.Error()
		ticketResponse.Error = true
		Render(w, r, ticketResponse, http.StatusBadRequest)
		return
	}

	ticketResponse.ID = ticket.ID
	ticketResponse.Name = ticket.Name
	ticketResponse.Price = ticket.Price
	ticketResponse.Quota = ticket.Quota
	ticketResponse.EventName = ticket.Event.Name
	ticketResponse.Location = ticket.Event.Location.Name
	ticketResponse.StartAt = ticket.Event.StartAt
	ticketResponse.EndAt = ticket.Event.EndAt

	Render(w, r, ticketResponse, http.StatusCreated)
}

//PostPurchaseTicket handler to purchase ticket
func (h *Handler) PostPurchaseTicket(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}

	r.ParseForm()

	var transactionRequest TransactionRequest

	transactionResponse := TransactionResponse{
		Response: Response{
			Error:  false,
			Object: "Transaction",
		},
	}

	body, err := GetBody(w, r, transactionRequest, transactionResponse)
	if err != nil {
		transactionResponse.Message = err.Error()
		transactionResponse.Error = true
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}

	req, err := json.Marshal(body)
	if err != nil {
		transactionResponse.Message = err.Error()
		transactionResponse.Error = true
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}
	json.Unmarshal(req, &transactionRequest)

	_, err = govalidator.ValidateStruct(transactionRequest)
	if err != nil {
		transactionResponse.Message = err.Error()
		transactionResponse.Error = true
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}

	customer, err := h.DB.CreateCustomer(transactionRequest.CustomerEmail, true)
	if err != nil {
		transactionResponse.Message = err.Error()
		transactionResponse.Error = true
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}

	var purchaseDetails []repository.PurchaseDetailsRequest

	for _, p := range transactionRequest.Purchases {
		purchaseDetails = append(purchaseDetails, repository.PurchaseDetailsRequest{
			TicketID: p.TicketID,
			Quantity: p.Quantity,
		})
	}

	transaction, err := h.DB.CreateTransaction(purchaseDetails, customer.ID)
	if err != nil {
		transactionResponse.Message = err.Error()
		transactionResponse.Error = true
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}

	transactionResponse = CreateTransactionResponse(transactionResponse, transaction)

	Render(w, r, transactionResponse, http.StatusCreated)
}

//TransactionDetail handler to get transaction detail
func (h *Handler) TransactionDetail(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.NotFound(w, r)
		return
	}

	transactionResponse := TransactionResponse{
		Response: Response{
			Error:  false,
			Object: "Transaction",
		},
	}

	transactionID := r.URL.Query().Get("transaction_id")
	id, err := strconv.Atoi(transactionID)
	if err != nil {
		transactionResponse.Message = err.Error()
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}

	if transactionID == "" {
		transactionResponse.Message = errors.New("Please provide transaction_id").Error()
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}

	transactionDetails, err := h.DB.GetTransactionDetailByID(id)
	if err != nil {
		transactionResponse.Message = err.Error()
		Render(w, r, transactionResponse, http.StatusBadRequest)
		return
	}

	transactionResponse = CreateTransactionResponse(transactionResponse, transactionDetails)

	Render(w, r, transactionResponse, http.StatusOK)
}

//GetEvent handler to get specific event
func (h *Handler) GetEvent(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.NotFound(w, r)
		return
	}

	eventResponse := EventResponse{
		Response: Response{
			Error:  false,
			Object: "Event",
		},
	}

	eventID := r.URL.Query().Get("event_id")
	id, err := strconv.Atoi(eventID)
	if err != nil {
		eventResponse.Message = err.Error()
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}

	if eventID == "" {
		eventResponse.Message = errors.New("Please provide event_id").Error()
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}

	event, err := h.DB.GetEventByID(id)
	if err != nil {
		eventResponse.Message = err.Error()
		Render(w, r, eventResponse, http.StatusBadRequest)
		return
	}

	eventResponse.ID = event.ID
	eventResponse.Name = event.Name
	eventResponse.Location = event.Location.Name
	eventResponse.StartAt = event.StartAt
	eventResponse.EndAt = event.EndAt

	Render(w, r, eventResponse, http.StatusOK)
}
