package server

import "time"

//LocationRequest for http request form or body
type LocationRequest struct {
	Name string `json:"name" valid:"required"`
}

//EventRequest for http request form or body
type EventRequest struct {
	Name     string    `json:"name" valid:"required"`
	Location string    `json:"location" valid:"required"`
	StartAt  time.Time `json:"start_at" valid:"required"`
	EndAt    time.Time `json:"end_at" valid:"required"`
}

//GetEventRequest for http get event query string
type GetEventRequest struct {
	EventID int `json:"event_id" valid:"required"`
}

//TicketRequest for http request form or body
type TicketRequest struct {
	EventID int    `json:"event_id" valid:"required"`
	Name    string `json:"name" valid:"required"`
	Price   int64  `json:"price" valid:"required"`
	Quota   int    `json:"quota" valid:"required"`
}

//PurchaseRequest detail
type PurchaseRequest struct {
	TicketID int `json:"ticket_id" valid:"required"`
	Quantity int `json:"quantity" valid:"required"`
}

//TransactionRequest for http requet form or body
type TransactionRequest struct {
	CustomerEmail string            `json:"customer_email" valid:"required"`
	Purchases     []PurchaseRequest `json:"purchases"`
}

//GetTransactionDetail for http get transaction detail query string
type GetTransactionDetail struct {
	TransactionID int `json:"transaction_id" valid:"required"`
}
