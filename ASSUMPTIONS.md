# LOKET

This project use minimal third party or external library:
- https://github.com/asaskevich/govalidator for request/struct validator
- https://github.com/lib/pq for postgres driver
- https://github.com/stretchr/testify for test assertion

- No third party mux/router or framework, only use standard library.
- No ORM, use raw sql.
- WARNING NO TRANSACTIONAL!

Postgres Database connection
- user = madgeek
- password = madgeek
- sslmode = disable


To running the http server
```sh
go run cmd/server/main.go
```
To running the test
```sh
go test ./...
```
note: only test http endpoint.

Example:
#### endpoint POST "/event/create"
```js
header: {
    "content-type": "application/json"
}

body:  {
        "name": "Konser",
        "location": "Jakarta",
        "start_at": "2019-04-25T18:25:43.511Z",
        "end_at": "2019-04-24T18:25:43.511Z"
}
```

#### endpoint POST "/location/create"
```js
header: {
    "content-type": "application/json"
}

body:  {
        "name": "Jakarta"
}
```

#### endpoint POST "/event/ticket/create"
```js
header: {
    "content-type": "application/json"
}

body:  {
	"name": "VIP",
	"event_id": 1,
	"price": 1000,
	"quota": 10
}
```

#### endpoint POST "/transaction/purchase"
```js
header: {
    "content-type": "application/json"
}

body:  {
    "customer_email": "aldidana@gmail.com",
    "purchases": [{
        "ticket_id": 1,
	    "quantity": 3
    }, {
        "ticket_id": 2,
	    "quantity": 4
    }]
}
```

#### endpoint GET "/event/get_info?event_id=1"
#### endpoint GET "/transaction/get_info?transaction_id=1"