package main

import (
	"net/http"

	"gitlab.com/aldidana/loket/repository"
	"gitlab.com/aldidana/loket/server"

	_ "github.com/lib/pq"
)

func main() {
	conn, err := repository.NewDBConnection("postgres", "user=madgeek dbname=madgeek sslmode=disable")
	if err != nil {
		panic(err)
	}

	h := server.NewHandler(conn)

	mux := http.NewServeMux()

	//POST
	mux.HandleFunc("/location/create", h.PostCreateLocation)
	mux.HandleFunc("/event/create", h.PostCreateEvent)
	mux.HandleFunc("/event/ticket/create", h.PostCreateTicket)
	mux.HandleFunc("/transaction/purchase", h.PostPurchaseTicket)

	//GET
	mux.HandleFunc("/event/get_info", h.GetEvent)
	mux.HandleFunc("/transaction/get_info", h.TransactionDetail)

	http.ListenAndServe(":4080", server.Logger(mux))
}
