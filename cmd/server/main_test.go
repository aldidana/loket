package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/aldidana/loket/repository"
	"gitlab.com/aldidana/loket/server"
)

var start, _ = time.Parse(time.RFC3339, "2019-04-05T11:45:26.371Z")
var end, _ = time.Parse(time.RFC3339, "2019-04-05T18:25:43.511Z")

var customer = &repository.Customer{
	ID:    1,
	Email: "aldidana@gmail.com",
}

var ticket1 = repository.Ticket{
	ID:   1,
	Name: "VIP",
	Event: repository.Event{
		ID:      1,
		Name:    "John Mayer",
		StartAt: &start,
		EndAt:   &end,
		Location: repository.Location{
			ID:   1,
			Name: "Jakarta",
		},
	},
	Price: 100000,
	Quota: 1000,
}

var ticket2 = repository.Ticket{
	ID:   2,
	Name: "VIP2",
	Event: repository.Event{
		ID:      1,
		Name:    "John Mayer",
		StartAt: &start,
		EndAt:   &end,
		Location: repository.Location{
			ID:   1,
			Name: "Jakarta",
		},
	},
	Price: 100000,
	Quota: 100,
}

var transaction = &repository.Transaction{
	ID:         1,
	Customer:   *customer,
	TotalPrice: 400000,
}

var purchases = []repository.Purchase{
	{
		ID:            1,
		TransactionID: 1,
		Ticket:        ticket1,
		Quantity:      2,
		TotalPrice:    200000,
	},
	{
		ID:            2,
		TransactionID: 1,
		Ticket:        ticket2,
		Quantity:      2,
		TotalPrice:    200000,
	},
}

var transactionDetails = &repository.TransactionDetails{
	Transaction: *transaction,
	Purchases:   purchases,
}

type mockDB struct{}

func (mdb *mockDB) CreateLocation(name string) (*repository.Location, error) {
	return &repository.Location{
		ID:   1,
		Name: "Jakarta",
	}, nil
}

func (mdb *mockDB) SearchLocation(name string) (*repository.Location, error) {
	return nil, nil
}

func (mdb *mockDB) SearchExactLocation(name string) (*repository.Location, error) {
	return nil, nil
}
func (mdb *mockDB) GetLocationByID(id int) (*repository.Location, error) {
	return nil, nil
}

func (mdb *mockDB) CreateEvent(name, location string, startAt, endAt time.Time) (*repository.Event, error) {
	return &repository.Event{
		ID:      1,
		Name:    "John Mayer",
		StartAt: &start,
		EndAt:   &end,
		Location: repository.Location{
			ID: 1, Name: "Jakarta",
		},
	}, nil
}

func (mdb *mockDB) GetEventByID(id int) (*repository.Event, error) {
	return nil, nil
}

func (mdb *mockDB) CreateTicket(name string, eventID int, price int64, quota int) (*repository.Ticket, error) {
	return &repository.Ticket{
		ID:   1,
		Name: "VIP",
		Event: repository.Event{
			ID:      1,
			Name:    "John Mayer",
			StartAt: &start,
			EndAt:   &end,
			Location: repository.Location{
				ID: 1, Name: "Jakarta",
			},
		},
		Price: 100000,
		Quota: 1000,
	}, nil
}

func (mdb *mockDB) GetTicketByID(id int) (*repository.Ticket, error) {
	return &repository.Ticket{
		ID:   1,
		Name: "VIP",
		Event: repository.Event{
			ID:      1,
			Name:    "John Mayer",
			StartAt: &start,
			EndAt:   &end,
			Location: repository.Location{
				ID: 1, Name: "Jakarta",
			},
		},
		Price: 100000,
		Quota: 1000,
	}, nil
}

func (mdb *mockDB) CreateCustomer(email string, purchase bool) (*repository.Customer, error) {
	return customer, nil
}

func (mdb *mockDB) CreateTransaction(purchaseDetails []repository.PurchaseDetailsRequest, customerID int) (*repository.TransactionDetails, error) {
	return transactionDetails, nil
}

func (mdb *mockDB) GetCustomerByEmail(email string) (*repository.Customer, error) {
	return customer, nil
}

func (mdb *mockDB) GetTransactionByID(id int) (*repository.Transaction, error) {
	return transaction, nil
}

func (mdb *mockDB) GetTransactionDetailByID(id int) (*repository.TransactionDetails, error) {
	return transactionDetails, nil
}

func (mdb *mockDB) UpdateTransactionTotalPriceByID(id int, totalPrice int64) (*repository.Transaction, error) {
	return transaction, nil
}

func (mdb *mockDB) CreatePurchaseDetails(transactionID, ticketID, quantity int, totalPrice int64) (*repository.Purchase, error) {
	return &purchases[0], nil
}

func (mdb *mockDB) GetPurchaseDetailsByID(id int) (*repository.Purchase, error) {
	return &purchases[0], nil
}

func TestCreateLocation(t *testing.T) {
	var createLocationTests = []struct {
		input    []byte
		expected server.LocationResponse
	}{
		{
			[]byte(`{"name": "jakarta"}`),
			server.LocationResponse{
				Response: server.Response{
					Error:  false,
					Object: "Location",
				},
				ID:   1,
				Name: "Jakarta",
			},
		}, {
			[]byte(`{"name":""}`),
			server.LocationResponse{
				Response: server.Response{
					Error:   true,
					Object:  "Location",
					Message: "name: non zero value required",
				},
			}},
	}

	for _, postData := range createLocationTests {
		rec := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/location/create", bytes.NewBuffer(postData.input))
		req.Header.Set("Content-Type", "application/json")

		h := server.Handler{DB: &mockDB{}}

		http.HandlerFunc(h.PostCreateLocation).ServeHTTP(rec, req)

		var actual server.LocationResponse

		json.Unmarshal([]byte(rec.Body.String()), &actual)

		assert.Equal(t, postData.expected, actual)
	}
}

func TestCreateEvent(t *testing.T) {
	startAt, _ := time.Parse(time.RFC3339, "2019-04-05T11:45:26.371Z")
	endAt, _ := time.Parse(time.RFC3339, "2019-04-05T18:25:43.511Z")

	var createEventTests = []struct {
		input    []byte
		expected server.EventResponse
	}{
		{
			[]byte(`{
				"name": "John Mayer",
				"location": "Jakarta",
				"start_at": "2019-04-05T11:45:26.371Z",
				"end_at": "2019-04-05T18:25:43.511Z"
			}`),
			server.EventResponse{
				Response: server.Response{
					Error:  false,
					Object: "Event",
				},
				ID:       1,
				Name:     "John Mayer",
				Location: "Jakarta",
				StartAt:  &startAt,
				EndAt:    &endAt,
			},
		},
		{
			[]byte(`{
				"location": "jakarta",
				"start_at": "2012-04-23T18:25:43.511Z",
				"end_at": "2012-04-24T18:25:43.511Z"
			}`),
			server.EventResponse{
				Response: server.Response{
					Error:   true,
					Object:  "Event",
					Message: "name: non zero value required",
				},
			},
		},
	}
	for _, postData := range createEventTests {
		rec := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/event/create", bytes.NewBuffer(postData.input))
		req.Header.Set("Content-Type", "application/json")

		h := server.Handler{DB: &mockDB{}}

		http.HandlerFunc(h.PostCreateEvent).ServeHTTP(rec, req)

		var actual server.EventResponse

		json.Unmarshal([]byte(rec.Body.String()), &actual)

		assert.Equal(t, postData.expected, actual)
	}
}

func TestCreateTicket(t *testing.T) {
	var createTicketTests = []struct {
		input    []byte
		expected server.TicketResponse
	}{
		{
			[]byte(`{
				"name": "VIP",
				"event_id": 1,
				"price": 1000,
				"quota": 10
			}`),
			server.TicketResponse{
				Response: server.Response{
					Error:  false,
					Object: "Ticket",
				},
				ID:        1,
				Name:      "VIP",
				Price:     100000,
				Quota:     1000,
				EventName: "John Mayer",
				Location:  "Jakarta",
				StartAt:   &start,
				EndAt:     &end,
			},
		}, {
			[]byte(`{"name":""}`),
			server.TicketResponse{
				Response: server.Response{
					Error:  true,
					Object: "Ticket",
				},
			}},
	}

	for _, postData := range createTicketTests {
		rec := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/event/ticket/create", bytes.NewBuffer(postData.input))
		req.Header.Set("Content-Type", "application/json")

		h := server.Handler{DB: &mockDB{}}

		http.HandlerFunc(h.PostCreateTicket).ServeHTTP(rec, req)

		var actual server.TicketResponse

		json.Unmarshal([]byte(rec.Body.String()), &actual)

		assert.Equal(t, postData.expected.Name, actual.Name)
		assert.Equal(t, postData.expected.EventName, actual.EventName)
		assert.Equal(t, postData.expected.Price, actual.Price)
		assert.Equal(t, postData.expected.Quota, actual.Quota)
		assert.Equal(t, postData.expected.Location, actual.Location)
		assert.Equal(t, postData.expected.StartAt, actual.StartAt)
		assert.Equal(t, postData.expected.EndAt, actual.EndAt)
	}
}

func TestCreateTransaction(t *testing.T) {
	var createTransactionTest = []struct {
		input    []byte
		expected server.TransactionResponse
	}{
		{
			[]byte(`{
				"ticket_id": 3,
				"quantity": 2,
				"customer_email": "aldidana@gmail.com"
			}`),
			server.TransactionResponse{
				Response: server.Response{
					Error:  false,
					Object: "Transaction",
				},
				ID:            1,
				CustomerEmail: "aldidana@gmail.com",
				GrandTotal:    400000,
				PurchasesDetails: []server.PurchaseResponse{
					{
						EventName:     "John Mayer",
						EventLocation: "Jakarta",
						EventStart:    &start,
						EventEnd:      &end,
						TicketName:    "VIP",
						TicketPrice:   100000,
						Quantity:      2,
						TotalPrice:    200000,
					},
					{
						EventName:     "John Mayer",
						EventLocation: "Jakarta",
						EventStart:    &start,
						EventEnd:      &end,
						TicketName:    "VIP2",
						TicketPrice:   100000,
						Quantity:      2,
						TotalPrice:    200000,
					},
				},
			},
		}, {
			[]byte(`{"name":""}`),
			server.TransactionResponse{
				Response: server.Response{
					Error:  true,
					Object: "Transaction",
				},
			}},
	}

	for _, postData := range createTransactionTest {
		rec := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/transaction/purchase", bytes.NewBuffer(postData.input))
		req.Header.Set("Content-Type", "application/json")

		h := server.Handler{DB: &mockDB{}}

		http.HandlerFunc(h.PostPurchaseTicket).ServeHTTP(rec, req)

		var actual server.TransactionResponse

		json.Unmarshal([]byte(rec.Body.String()), &actual)

		assert.Equal(t, postData.expected.CustomerEmail, actual.CustomerEmail)
		assert.Equal(t, postData.expected.PurchasesDetails, actual.PurchasesDetails)
		assert.Equal(t, postData.expected.GrandTotal, actual.GrandTotal)
	}
}
