package repository

import (
	"database/sql"
	"errors"
	"time"
)

//Event data
type Event struct {
	ID       int        `json:"id"`
	Name     string     `json:"name"`
	StartAt  *time.Time `json:"start_at"`
	EndAt    *time.Time `json:"end_at"`
	Location Location   `json:"location"`
}

//CreateEvent to create event
func (conn *Conn) CreateEvent(name, location string, startAt, endAt time.Time) (*Event, error) {
	loc, err := conn.SearchExactLocation(location)
	if err != nil {
		return nil, err
	}

	if loc == nil {
		loc, err = conn.CreateLocation(location)
		if err != nil {
			return nil, err
		}
	}

	insertStatement := `
		INSERT INTO events(name, location_id, start_at, end_at)
		VALUES($1, $2, $3, $4)
		RETURNING id`

	var currentID int

	err = conn.DB.QueryRow(insertStatement, name, loc.ID, startAt, endAt).Scan(&currentID)
	if err != nil {
		return nil, err
	}

	return conn.GetEventByID(currentID)
}

//GetEventByID to get created event by ID
func (conn *Conn) GetEventByID(id int) (*Event, error) {
	row := conn.DB.QueryRow(`
		SELECT events.id, events.name, events.start_at, events.end_at, events.location_id, locations.name as location_name
		FROM events JOIN locations on events.location_id = locations.id WHERE events.id=($1)`, id)

	var event Event
	err := row.Scan(&event.ID, &event.Name, &event.StartAt, &event.EndAt, &event.Location.ID, &event.Location.Name)
	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
		return nil, errors.New("Record/Data not found")
	}

	return &event, nil
}
