package repository

import (
	"database/sql"
	"time"
)

//Datastore interface
type Datastore interface {
	CreateLocation(name string) (*Location, error)
	GetLocationByID(id int) (*Location, error)
	CreateEvent(name, location string, startAt, endAt time.Time) (*Event, error)
	GetEventByID(id int) (*Event, error)
	CreateTicket(name string, eventID int, price int64, quota int) (*Ticket, error)
	GetTicketByID(id int) (*Ticket, error)
	CreateTransaction(purchaseDetails []PurchaseDetailsRequest, customerID int) (*TransactionDetails, error)
	CreateCustomer(email string, purchase bool) (*Customer, error)
	GetCustomerByEmail(email string) (*Customer, error)
	GetTransactionByID(id int) (*Transaction, error)
	GetTransactionDetailByID(id int) (*TransactionDetails, error)
	UpdateTransactionTotalPriceByID(id int, totalPrice int64) (*Transaction, error)
	CreatePurchaseDetails(transactionID, ticketID, quantity int, totalPrice int64) (*Purchase, error)
	GetPurchaseDetailsByID(id int) (*Purchase, error)
}

//Conn database connection
type Conn struct {
	*sql.DB
}

//NewDBConnection to create new connection to database
func NewDBConnection(driverName, dataSourceName string) (*Conn, error) {
	db, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return &Conn{db}, nil
}
