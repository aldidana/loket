package repository

import (
	"database/sql"
	"strings"
)

//Location Type
type Location struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

//SearchLocation by location name (case insensitive) using ilike
func (conn *Conn) SearchLocation(name string) (*Location, error) {
	row := conn.DB.QueryRow(`SELECT * FROM locations WHERE name ILIKE '%' || ($1) || '%'`, name)

	var locaction Location
	err := row.Scan(&locaction.ID, &locaction.Name)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	return &locaction, nil
}

//SearchExactLocation by location name
func (conn *Conn) SearchExactLocation(name string) (*Location, error) {
	name = strings.Title(name)
	row := conn.DB.QueryRow(`SELECT * FROM locations WHERE name=($1)`, name)

	var locaction Location
	err := row.Scan(&locaction.ID, &locaction.Name)
	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
		return nil, nil
	}

	return &locaction, nil
}

//GetLocationByID by location name
func (conn *Conn) GetLocationByID(id int) (*Location, error) {
	row := conn.DB.QueryRow(`SELECT * FROM locations WHERE id=($1)`, id)

	var location Location
	err := row.Scan(&location.ID, &location.Name)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	return &location, nil
}

//CreateLocation insert location by name if location name doesn't exists
func (conn *Conn) CreateLocation(name string) (*Location, error) {
	name = strings.Title(name)
	insertStatement := `
		INSERT INTO locations (name)
		VALUES ($1)
		RETURNING id`

	var currentID int

	err := conn.DB.QueryRow(insertStatement, name).Scan(&currentID)
	if err != nil {
		return nil, err
	}

	return conn.GetLocationByID(currentID)
}
