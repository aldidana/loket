package repository

import (
	"database/sql"
	"errors"
	"fmt"
)

//Transaction data
type Transaction struct {
	ID int `json:"id,omitempty"`
	// Ticket     Ticket   `json:"ticket,omitempty"`
	Customer   Customer `json:"customer,omitempty"`
	TotalPrice int64    `json:"total_price,omitempty"`
	// Quantity int `json:"quantity,omitempty"`
}

//Purchase details
type Purchase struct {
	ID            int    `json:"id,omitempty"`
	TransactionID int    `json:"transaction_id,omitempty"`
	Ticket        Ticket `json:"ticket,omitempty"`
	Quantity      int    `json:"quantity,omitempty"`
	TotalPrice    int64  `json:"total_price,omitempty"`
}

//TransactionDetails for list of purchases in one transaction
type TransactionDetails struct {
	Transaction
	Purchases []Purchase `json:"purchases,omitempty"`
}

//PurchaseDetailsRequest detail of purchases request
type PurchaseDetailsRequest struct {
	TicketID int `json:"ticket_id,omitempty"`
	Quantity int `json:"quantity,omitempty"`
}

//CreateTransaction to create ticket types
func (conn *Conn) CreateTransaction(purchaseDetails []PurchaseDetailsRequest, customerID int) (*TransactionDetails, error) {
	insertStatement := `
		INSERT INTO transactions(customer_id, total_price)
		VALUES($1, $2)
		RETURNING id`

	var currentID int

	err := conn.DB.QueryRow(insertStatement, customerID, 0).Scan(&currentID)
	if err != nil {
		return nil, err
	}

	// transaction, err := conn.GetTransactionByID(currentID)
	// if err != nil {
	// 	return nil, err
	// }

	var purchases []Purchase
	var grandTotal int64

	for _, p := range purchaseDetails {
		ticket, err := conn.GetTicketByID(p.TicketID)
		if err != nil {
			return nil, err
		}

		if ticket == nil {
			return nil, errors.New("Ticket not found")
		}

		if p.Quantity > ticket.Quota {
			return nil, errors.New("Insufficient Quantity Remaining")
		}

		tPrice := int64(p.Quantity) * ticket.Price

		grandTotal += tPrice

		newAmount := ticket.Quota - p.Quantity
		_, err = conn.SubtractQuota(ticket.ID, newAmount)
		if err != nil {
			return nil, err
		}

		pr, err := conn.CreatePurchaseDetails(currentID, p.TicketID, p.Quantity, tPrice)
		if err != nil {
			return nil, err
		}

		purchases = append(purchases, *pr)
	}

	trx, err := conn.UpdateTransactionTotalPriceByID(currentID, grandTotal)
	if err != nil {
		return nil, err
	}

	return &TransactionDetails{
		Transaction: *trx,
		Purchases:   purchases,
	}, nil
}

// GetTransactionByID to get created transaction by ID
func (conn *Conn) GetTransactionByID(id int) (*Transaction, error) {
	row := conn.DB.QueryRow(`
		SELECT trx.id, customers.email, trx.total_price
		FROM transactions as trx
		JOIN customers on customers.id = trx.customer_id
		WHERE trx.id=($1)`, id)

	var transaction Transaction
	err := row.Scan(&transaction.ID, &transaction.Customer.Email, &transaction.TotalPrice)

	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
		return nil, errors.New("Record/Data not found")
	}

	return &transaction, nil
}

// GetTransactionDetailByID to get created transaction detail with purchases details by ID
func (conn *Conn) GetTransactionDetailByID(id int) (*TransactionDetails, error) {
	row := conn.DB.QueryRow(`
	SELECT trx.id, customers.email, trx.total_price
	FROM transactions as trx
	JOIN customers on customers.id = trx.customer_id
	WHERE trx.id=($1)`, id)

	var transaction Transaction
	err := row.Scan(&transaction.ID, &transaction.Customer.Email, &transaction.TotalPrice)

	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
		return nil, errors.New("Record/Data not found")
	}

	rows, err := conn.DB.Query(`
		SELECT purchase_details.id
		FROM purchase_details
		WHERE purchase_details.transaction_id=($1)
	`, id)

	if err != nil {
		return nil, err
	}

	var ids []int
	defer rows.Close()

	for rows.Next() {
		var currentID int
		err = rows.Scan(&currentID)
		if err != nil {
			//just log
			fmt.Println("Purchase details not found or error")
			break
		}

		ids = append(ids, currentID)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	var purchases []Purchase

	for _, currentID := range ids {
		purchase, err := conn.GetPurchaseDetailsByID(currentID)
		if err != nil {
			return nil, err
		}
		purchases = append(purchases, *purchase)
	}

	return &TransactionDetails{
		Transaction: transaction,
		Purchases:   purchases,
	}, nil
}

// UpdateTransactionTotalPriceByID to update current transaction total price by id
func (conn *Conn) UpdateTransactionTotalPriceByID(id int, totalPrice int64) (*Transaction, error) {
	updateStatement := `
		UPDATE transactions SET total_price = ($1)
		WHERE transactions.id=($2)
		RETURNING id
	`

	var currentID int
	err := conn.DB.QueryRow(updateStatement, totalPrice, id).Scan(&currentID)
	if err != nil {
		return nil, err
	}

	return conn.GetTransactionByID(currentID)
}

//CreatePurchaseDetails to create a purchase detail from current transaction id
func (conn *Conn) CreatePurchaseDetails(transactionID, ticketID, quantity int, totalPrice int64) (*Purchase, error) {
	insertStatement := `
		INSERT INTO purchase_details(transaction_id, ticket_id, quantity, total_price)
		VALUES($1, $2, $3, $4)
		RETURNING id`

	var currentID int
	err := conn.DB.QueryRow(insertStatement, transactionID, ticketID, quantity, totalPrice).Scan(&currentID)
	if err != nil {
		return nil, err
	}

	return conn.GetPurchaseDetailsByID(currentID)
}

//GetPurchaseDetailsByID to get a purchase detail by id
func (conn *Conn) GetPurchaseDetailsByID(id int) (*Purchase, error) {
	row := conn.DB.QueryRow(`
		SELECT purchase.id, purchase.transaction_id, purchase.quantity, purchase.total_price,
		ticket.name, ticket.price,
		locations.name,
		events.name, events.start_at, events.end_at
		FROM purchase_details as purchase
		JOIN ticket_types as ticket on ticket.id = purchase.ticket_id
		JOIN events on events.id = ticket.event_id
		JOIN locations on locations.id = events.location_id
		WHERE purchase.id=($1)`, id)

	var purchase Purchase
	err := row.Scan(&purchase.ID, &purchase.TransactionID, &purchase.Quantity, &purchase.TotalPrice,
		&purchase.Ticket.Name, &purchase.Ticket.Price,
		&purchase.Ticket.Event.Location.Name,
		&purchase.Ticket.Event.Name, &purchase.Ticket.Event.StartAt, &purchase.Ticket.Event.EndAt)

	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
		return nil, errors.New("Record/Data not found")
	}

	return &purchase, nil
}
