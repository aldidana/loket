package repository

import (
	"database/sql"
	"errors"
)

//Ticket data
type Ticket struct {
	ID    int    `json:"id,omitempty"`
	Name  string `json:"name,,omitempty"`
	Event Event  `json:"event,omitempty"`
	Price int64  `json:"price,omitempty"`
	Quota int    `json:"quota,omitempty"`
}

//CreateTicket to create ticket types
func (conn *Conn) CreateTicket(name string, eventID int, price int64, quota int) (*Ticket, error) {
	event, err := conn.GetEventByID(eventID)
	if err != nil {
		return nil, err
	}

	if event == nil {
		return nil, errors.New("Event not found, please create event first")
	}

	insertStatement := `
		INSERT INTO ticket_types(name, event_id, price, quota)
		VALUES($1, $2, $3, $4)
		RETURNING id`

	var currentID int

	err = conn.DB.QueryRow(insertStatement, name, eventID, price, quota).Scan(&currentID)
	if err != nil {
		return nil, err
	}

	return conn.GetTicketByID(currentID)
}

//GetTicketByID to get created ticket by ID
func (conn *Conn) GetTicketByID(id int) (*Ticket, error) {
	row := conn.DB.QueryRow(`
		SELECT ticket.id, ticket.name, ticket.price, ticket.quota, events.name as event_name, events.start_at, events.end_at, locations.name as location_name
		FROM ticket_types as ticket
		JOIN events on events.id = ticket.event_id
		JOIN locations on locations.id = events.location_id
		WHERE ticket.id=($1)`, id)

	var ticket Ticket
	err := row.Scan(&ticket.ID, &ticket.Name, &ticket.Price, &ticket.Quota, &ticket.Event.Name, &ticket.Event.StartAt, &ticket.Event.EndAt, &ticket.Event.Location.Name)
	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
		return nil, errors.New("Record/Data not found")
	}

	return &ticket, nil
}

// SubtractQuota by defined newAmount
func (conn *Conn) SubtractQuota(id, newAmount int) (*Ticket, error) {
	updateStatement := `
		UPDATE ticket_types SET quota = ($1)
		WHERE ticket_types.id = ($2)
		RETURNING id
	`

	var currentID int

	err := conn.DB.QueryRow(updateStatement, newAmount, id).Scan(&currentID)
	if err != nil {
		return nil, err
	}

	return conn.GetTicketByID(currentID)
}
