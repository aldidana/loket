package repository

import (
	"database/sql"
	"errors"
)

//Customer data
type Customer struct {
	ID    int    `json:"id,omitempty"`
	Email string `json:"email,omitempty"`
}

//CreateCustomer to create ticket types, if purchase is true won't throw error even user already exists
func (conn *Conn) CreateCustomer(email string, purchase bool) (*Customer, error) {
	customer, err := conn.GetCustomerByEmail(email)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	if customer != nil {
		if purchase {
			return customer, nil
		}

		return nil, errors.New("Customer/Email already exists")
	}

	insertStatement := `
		INSERT INTO customers(email)
		VALUES($1)
		RETURNING id`

	_, err = conn.DB.Exec(insertStatement, email)
	if err != nil {
		return nil, err
	}

	return conn.GetCustomerByEmail(email)
}

//GetCustomerByEmail to get created customer by email
func (conn *Conn) GetCustomerByEmail(email string) (*Customer, error) {
	row := conn.DB.QueryRow(`
		SELECT *
		FROM customers
		WHERE customers.email=($1)`, email)

	var customer Customer
	err := row.Scan(&customer.ID, &customer.Email)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return &customer, nil
}
